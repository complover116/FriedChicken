package com.complover116.friedchicken.tileentities;

import com.complover116.friedchicken.FriedChickenMod;
import com.complover116.friedchicken.Holder;
import net.minecraft.block.BlockLever;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ITickable;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class ChickenProcessor extends TileEntity implements ITickable {
    private int chickensLeft = 0;
    private int progress = -1;
    private static final int PROCESSING_TIME = 10;
    private int anim = 0;

    @Override
    public void update() {
        if (world.isRemote) return;
        if (progress == -1) return;
        progress++;
        if (progress == PROCESSING_TIME/2) {
            if (chickensLeft > 0) {
                setAnim(1);
            } else {
                setAnim(0);
                progress = -1;
            }
        }
        if (progress >= PROCESSING_TIME) {
            progress = 0;
            chickensLeft --;
            setAnim(2);
            world.playSound(null, pos, SoundEvents.ENTITY_CHICKEN_HURT,  SoundCategory.BLOCKS, 0.5f, (float) (Math.random()*0.5f + 0.75f));
            world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_PLACE,  SoundCategory.BLOCKS, 0.5f, (float) (Math.random()*0.5f + 0.75f));
            dropItemStack(new ItemStack(Holder.chicken_leg, 2));
            dropItemStack(new ItemStack(Holder.chicken_wing, 2));
            dropItemStack(new ItemStack(Holder.chicken_breast, 1));
        }
    }

    private void setAnim(int newAnim) {
        boolean changed = anim != newAnim;
        this.anim = newAnim;
        if (changed) {
            markDirty();
            final IBlockState state = getWorld().getBlockState(getPos());
            getWorld().notifyBlockUpdate(getPos(), state, state, 3);
        }
    }
    public int getAnim() {
        return anim;
    }

    void dropItemStack(ItemStack itemStack) {
        EntityItem entity = new EntityItem(world, pos.getX(), pos.getY(), pos.getZ()+1f, itemStack);
        world.spawnEntity(entity);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        NBTTagCompound tag = pkt.getNbtCompound();
        this.anim = tag.getInteger("anim");
        final IBlockState state = getWorld().getBlockState(getPos());
        getWorld().notifyBlockUpdate(getPos(), state, state, 3);
    }

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("anim", anim);
        return new SPacketUpdateTileEntity(getPos(), -1, tag);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setInteger("anim", anim);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        this.anim = tag.getInteger("anim");
    }

    public void addChickens(int itemCount) {
        chickensLeft += itemCount;
        if (progress == -1) {
            progress = 0;
            setAnim(1);
        }
    }
}
