package com.complover116.friedchicken.tileentities;

import com.complover116.friedchicken.FriedChickenMod;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class TilePackOfCans extends TileEntity {
    private int canCount = 6;

    public TilePackOfCans() {

    }


    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.canCount = compound.getInteger("canCount");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setInteger("canCount", canCount);
        return super.writeToNBT(compound);
    }

    @Override
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate) {
        return oldState.getBlock() != newSate.getBlock();
    }

    public boolean isCanPresent(int canID) {
        return canCount > canID;
    }

    public void takeCan() {
        this.canCount --;
        this.markDirty();
    }

    public boolean placeCan() {
        if (canCount >= 6) return false;
        this.canCount ++;
        this.markDirty();
        return true;
    }

    public void setCanCount(int count) {
        this.canCount = count;
        this.markDirty();
    }

    public void checkDespawn() {
        if (canCount <= 0) {
            getWorld().setBlockToAir(getPos());
        }
    }

    public int getCanCount() {
        return canCount;
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        NBTTagCompound tag = pkt.getNbtCompound();
        this.canCount = tag.getInteger("canCount");
        final IBlockState state = getWorld().getBlockState(getPos());
        getWorld().notifyBlockUpdate(getPos(), state, state, 3);
    }

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("canCount", canCount);
        return new SPacketUpdateTileEntity(getPos(), -1, tag);
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setInteger("canCount", canCount);
        return tag;
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);
        this.canCount = tag.getInteger("canCount");
    }
}
