package com.complover116.friedchicken;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@GameRegistry.ObjectHolder("friedchickenmod")
public class Holder {
    public static final Item chicken_leg = null;
    public static final Item chicken_leg_fried = null;
    public static final Item chicken_wing = null;
    public static final Item chicken_wing_fried = null;
    public static final Item chicken_breast = null;
    public static final Item chicken_breast_fried = null;
    public static final Block chicken_processor = null;
    public static final Block green_monster_pack = null;
    public static final Block yellow_monster_pack = null;

    public static final Item bucket_empty = null;
    public static final Item bucket_wings = null;

    public static final Item empty_can = null;
    public static final Item sugar_pack = null;
    public static final Item green_monster = null;
    public static final Item yellow_monster = null;

    @GameRegistry.ObjectHolder("chicken_processor")
    public static final Item chicken_processor_item = null;

    @GameRegistry.ObjectHolder("green_monster_pack")
    public static final Item green_monster_pack_item = null;

    @GameRegistry.ObjectHolder("yellow_monster_pack")
    public static final Item yellow_monster_pack_item = null;

    public static final SoundEvent can_open = null;
    public static final SoundEvent can_drink = null;
    public static final SoundEvent can_take = null;
}
