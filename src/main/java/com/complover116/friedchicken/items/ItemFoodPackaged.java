package com.complover116.friedchicken.items;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemFoodPackaged extends ItemFastFood {
    final Item emptyPackage;

    public ItemFoodPackaged(int amount, float saturation, Item emptyPackage, int useCount) {
        super(amount, saturation, useCount);
        this.emptyPackage = emptyPackage;
        setMaxDamage(useCount);
    }

    @Override
    protected void onFinished(ItemStack stack, World worldIn, EntityPlayer player) {
        if (worldIn.isRemote) return;
        if (!player.inventory.addItemStackToInventory(new ItemStack(emptyPackage))) {
            EntityItem entity = new EntityItem(worldIn, player.posX, player.posY + 1f, player.posZ, new ItemStack(emptyPackage));
            worldIn.spawnEntity(entity);
        }
    }
}
