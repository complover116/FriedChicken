package com.complover116.friedchicken.items;

import com.complover116.friedchicken.FriedChickenMod;
import com.complover116.friedchicken.Holder;
import com.complover116.friedchicken.blocks.BlockPackOfCans;
import com.complover116.friedchicken.tileentities.TilePackOfCans;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumAction;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class ItemDrink extends Item {
    final Item emptyPackage;
    final int canType;

    public ItemDrink(int useCount, Item emptyPackage, int canType) {
        super();
        this.canType = canType;
        setMaxDamage(useCount);
        this.emptyPackage = emptyPackage;
        setMaxStackSize(1);
        this.addPropertyOverride(new ResourceLocation("friedchickenmod:opened"), new IItemPropertyGetter() {
            @Override
            public float apply(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entityIn) {
                return stack.hasTagCompound() && stack.getTagCompound().getBoolean("friedchickenmod:opened") ? 1 : 0;
            }
        });
    }

    @Override
    public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
        super.onCreated(stack, worldIn, playerIn);
        initTags(stack);
    }

    @Override
    public EnumAction getItemUseAction(ItemStack stack) {
        initTags(stack);

        if (!stack.getTagCompound().getBoolean("friedchickenmod:opened"))
            return EnumAction.BOW;
        else
            return EnumAction.NONE;
    }

    private void initTags(ItemStack stack) {
        if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
            stack.getTagCompound().setBoolean("friedchickenmod:opened", false);
            stack.getTagCompound().setInteger("friedchickenmod:starting_damage", 0);
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand)
    {
        ItemStack itemstack = player.getHeldItem(hand);
        if (!(itemstack.getItem() instanceof ItemDrink)) {
            return new ActionResult<ItemStack>(EnumActionResult.FAIL, itemstack);
        }
        initTags(itemstack);
        player.setActiveHand(hand);
        itemstack.getTagCompound().setInteger("friedchickenmod:starting_damage", itemstack.getItemDamage());
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        BlockPackOfCans resultBlock = null;
        switch (canType) {
            case 0:
                resultBlock = (BlockPackOfCans) Holder.green_monster_pack;
                break;
            case 1:
                resultBlock = (BlockPackOfCans) Holder.yellow_monster_pack;
                break;
        }
        ItemStack itemstack = player.getHeldItem(hand);
        initTags(itemstack);

        if (itemstack.getTagCompound().getBoolean("friedchickenmod:opened")) {
            return super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
        }

        worldIn.playSound(player, pos, Holder.can_take, SoundCategory.BLOCKS, 1f, 1f);

        BlockPos placementPos = pos.offset(facing);
        worldIn.setBlockState(placementPos, resultBlock.getDefaultState());

        TilePackOfCans tileEntity = (TilePackOfCans) worldIn.getTileEntity(placementPos);
        tileEntity.setCanCount(1);
        worldIn.notifyBlockUpdate(placementPos, resultBlock.getDefaultState(), resultBlock.getActualState(resultBlock.getDefaultState(), worldIn, placementPos), 3);
        itemstack.shrink(1);
        return super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
    }

    public int getMaxItemUseDuration(ItemStack stack)
    {
        initTags(stack);
        if (!stack.getTagCompound().getBoolean("friedchickenmod:opened")) {
            return 16;
        } else {
            return 16;
        }
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving) {
        if (!(entityLiving instanceof EntityPlayer)) {
            return stack;
        }
        EntityPlayer player = (EntityPlayer) entityLiving;
        if (!stack.getTagCompound().getBoolean("friedchickenmod:opened")) {
            stack.getTagCompound().setBoolean("friedchickenmod:opened", true);
            player.playSound(Holder.can_open, 1f, 1f);
            return stack;
        } else {
            stack.setItemDamage(stack.getItemDamage() + 1);
            player.playSound(Holder.can_drink, 1f, (float) (Math.random()*0.5 + 0.75));

            if (!worldIn.isRemote) {
                player.getFoodStats().addStats(1, 2);
                player.addPotionEffect(new PotionEffect(Potion.getPotionById(1), 600));
            }
            if (stack.getItemDamage() >= stack.getMaxDamage()) {
                stack.shrink(1);
                if (!worldIn.isRemote) {
                    if (!player.inventory.addItemStackToInventory(new ItemStack(emptyPackage))) {
                        EntityItem entity = new EntityItem(worldIn, player.posX, player.posY + 1f, player.posZ, new ItemStack(emptyPackage));
                        worldIn.spawnEntity(entity);
                    }
                }
            }
        }
        return super.onItemUseFinish(stack, worldIn, entityLiving);
    }
}
