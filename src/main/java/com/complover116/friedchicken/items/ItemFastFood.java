package com.complover116.friedchicken.items;

import com.complover116.friedchicken.FriedChickenMod;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

public class ItemFastFood extends ItemFood {
    boolean isMultiUse;
    public ItemFastFood(int amount, float saturation, int useCount) {
        super(amount, saturation, false);
        if (useCount != 1) {
            setMaxStackSize(1);
            setMaxDamage(useCount);
            isMultiUse = true;
        } else {
            isMultiUse = false;
        }
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving)
    {
        if (entityLiving instanceof EntityPlayer)
        {
            EntityPlayer entityplayer = (EntityPlayer)entityLiving;
            entityplayer.getFoodStats().addStats(this, stack);
            worldIn.playSound((EntityPlayer)null, entityplayer.posX, entityplayer.posY, entityplayer.posZ, SoundEvents.ENTITY_PLAYER_BURP, SoundCategory.PLAYERS, 0.5F, worldIn.rand.nextFloat() * 0.1F + 0.9F);
            this.onFoodEaten(stack, worldIn, entityplayer);
            entityplayer.addStat(StatList.getObjectUseStats(this));

            if (entityplayer instanceof EntityPlayerMP)
            {
                CriteriaTriggers.CONSUME_ITEM.trigger((EntityPlayerMP)entityplayer, stack);
            }
        }

        if (!worldIn.isRemote) {
            if (!isMultiUse) {
                stack.shrink(1);
                onFinished(stack, worldIn, (EntityPlayer) entityLiving);
            } else {
                stack.setItemDamage(stack.getItemDamage() + 1);
                if (stack.getItemDamage() >= stack.getMaxDamage()) {
                    onFinished(stack, worldIn, (EntityPlayer) entityLiving);
                    stack.shrink(1);
                }
            }
        }
        return stack;
    }

    protected void onFinished(ItemStack stack, World worldIn, EntityPlayer player) {};

    @Override
    protected void onFoodEaten(ItemStack stack, World worldIn, EntityPlayer player) {
        super.onFoodEaten(stack, worldIn, player);
    }
}
