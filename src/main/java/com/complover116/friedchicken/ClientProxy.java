package com.complover116.friedchicken;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraftforge.client.model.ModelLoader;

public class ClientProxy extends CommonProxy {
    @Override
    public void registerModels() {
        ModelLoader.setCustomModelResourceLocation(Holder.chicken_leg, 0, new ModelResourceLocation("friedchickenmod:chicken_leg"));
        ModelLoader.setCustomModelResourceLocation(Holder.chicken_leg_fried, 0, new ModelResourceLocation("friedchickenmod:chicken_leg_fried"));
        ModelLoader.setCustomModelResourceLocation(Holder.chicken_wing, 0, new ModelResourceLocation("friedchickenmod:chicken_wing"));
        ModelLoader.setCustomModelResourceLocation(Holder.chicken_wing_fried, 0, new ModelResourceLocation("friedchickenmod:chicken_wing_fried"));
        ModelLoader.setCustomModelResourceLocation(Holder.chicken_breast, 0, new ModelResourceLocation("friedchickenmod:chicken_breast"));
        ModelLoader.setCustomModelResourceLocation(Holder.chicken_breast_fried, 0, new ModelResourceLocation("friedchickenmod:chicken_breast_fried"));
        ModelLoader.setCustomModelResourceLocation(Holder.chicken_processor_item, 0, new ModelResourceLocation("friedchickenmod:chicken_processor"));
        ModelLoader.setCustomModelResourceLocation(Holder.green_monster_pack_item, 0, new ModelResourceLocation("friedchickenmod:green_monster_pack"));
        ModelLoader.setCustomModelResourceLocation(Holder.yellow_monster_pack_item, 0, new ModelResourceLocation("friedchickenmod:yellow_monster_pack"));
        ModelLoader.setCustomModelResourceLocation(Holder.bucket_empty, 0, new ModelResourceLocation("friedchickenmod:bucket_empty"));
        ModelLoader.setCustomModelResourceLocation(Holder.bucket_wings, 0, new ModelResourceLocation("friedchickenmod:bucket_full"));
        ModelLoader.setCustomModelResourceLocation(Holder.green_monster, 0, new ModelResourceLocation("friedchickenmod:green_monster"));
        ModelLoader.setCustomModelResourceLocation(Holder.yellow_monster, 0, new ModelResourceLocation("friedchickenmod:yellow_monster"));
        ModelLoader.setCustomModelResourceLocation(Holder.empty_can, 0, new ModelResourceLocation("friedchickenmod:empty_can"));
        ModelLoader.setCustomModelResourceLocation(Holder.sugar_pack, 0, new ModelResourceLocation("friedchickenmod:sugar_pack"));
    }
}

