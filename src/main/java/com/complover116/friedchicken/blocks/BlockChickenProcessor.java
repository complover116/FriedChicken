package com.complover116.friedchicken.blocks;

import com.complover116.friedchicken.FriedChickenMod;
import com.complover116.friedchicken.Holder;
import com.complover116.friedchicken.tileentities.ChickenProcessor;
import com.complover116.friedchicken.tileentities.TilePackOfCans;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockChickenProcessor extends Block implements ITileEntityProvider {
    static final PropertyInteger anim = PropertyInteger.create("anim", 0, 2);

    public BlockChickenProcessor() {
        super(Material.ROCK);
        setHardness(2f);
        setCreativeTab(CreativeTabs.MISC);
        setRegistryName("chicken_processor");
        setUnlocalizedName("friedchickenmod.chicken_processor");
        IBlockState state = this.getBlockState().getBaseState();
        setDefaultState(state.withProperty(anim, 0));
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new ChickenProcessor();
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, anim);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return 0;
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return getDefaultState();
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        ChickenProcessor tileEntity = (ChickenProcessor) worldIn.getTileEntity(pos);
        return state.withProperty(anim, tileEntity.getAnim());
    }

    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        if (worldIn.isRemote)
        {
            return true;
        }

        if (playerIn.inventory.mainInventory.get(playerIn.inventory.currentItem).getItem() == Items.CHICKEN) {
            int itemCount = playerIn.inventory.mainInventory.get(playerIn.inventory.currentItem).getCount();
            playerIn.inventory.mainInventory.set(playerIn.inventory.currentItem, ItemStack.EMPTY);
            ChickenProcessor tileEntity = (ChickenProcessor) worldIn.getTileEntity(pos);
            tileEntity.addChickens(itemCount);
        }
        return true;
    }


}
