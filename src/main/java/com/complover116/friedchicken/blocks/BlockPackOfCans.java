package com.complover116.friedchicken.blocks;

import com.complover116.friedchicken.FriedChickenMod;
import com.complover116.friedchicken.Holder;
import com.complover116.friedchicken.tileentities.TilePackOfCans;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLever;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.Random;

public class BlockPackOfCans extends Block implements ITileEntityProvider {
    static final PropertyBool can_1 = PropertyBool.create("can_1");
    static final PropertyBool can_2 = PropertyBool.create("can_2");
    static final PropertyBool can_3 = PropertyBool.create("can_3");
    static final PropertyBool can_4 = PropertyBool.create("can_4");
    static final PropertyBool can_5 = PropertyBool.create("can_5");
    static final PropertyBool can_6 = PropertyBool.create("can_6");

    final int canType;

    protected static final AxisAlignedBB AABB = new AxisAlignedBB(0, 0, 0.1, 1, 0.5, 0.9);

    public BlockPackOfCans(int canType) {
        super(Material.CAKE);
        setHardness(1f);
        this.canType = canType;
        setCreativeTab(CreativeTabs.FOOD);
        IBlockState state = this.getBlockState().getBaseState();
        setDefaultState(state.withProperty(can_1, true)
        .withProperty(can_2, true)
        .withProperty(can_3, true)
        .withProperty(can_4, true)
        .withProperty(can_5, true)
        .withProperty(can_6, true));
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
    {
        return AABB;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return AABB;
    }

    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        TilePackOfCans tileEntity = (TilePackOfCans) world.getTileEntity(pos);
        if (tileEntity.getCanCount() == 6) {
            switch (canType) {
                case 0:
                    drops.add(new ItemStack(Holder.green_monster_pack_item));
                    break;
                case 1:
                    drops.add(new ItemStack(Holder.yellow_monster_pack_item));
                    break;
            }
        } else {
            for (int i = 0; i < tileEntity.getCanCount(); i ++) {
                switch (canType) {
                    case 0:
                        drops.add(new ItemStack(Holder.green_monster));
                        break;
                    case 1:
                        drops.add(new ItemStack(Holder.yellow_monster));
                        break;
                }
            }
        }
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, can_1, can_2, can_3, can_4, can_5, can_6);
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        TilePackOfCans tileEntity = (TilePackOfCans) worldIn.getTileEntity(pos);
        return state
                .withProperty(can_1, tileEntity.isCanPresent(0))
                .withProperty(can_2, tileEntity.isCanPresent(1))
                .withProperty(can_3, tileEntity.isCanPresent(2))
                .withProperty(can_4, tileEntity.isCanPresent(3))
                .withProperty(can_5, tileEntity.isCanPresent(4))
                .withProperty(can_6, tileEntity.isCanPresent(5));

    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return getDefaultState();
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return 0;
    }

    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (worldIn.isRemote) {
            return true;
        }
        TilePackOfCans tileEntity = (TilePackOfCans) worldIn.getTileEntity(pos);
        ItemStack heldItem = playerIn.getHeldItem(EnumHand.MAIN_HAND);
        if (playerIn.getHeldItem(EnumHand.MAIN_HAND).isEmpty()) {
            tileEntity.takeCan();
            worldIn.playSound(null, pos, Holder.can_take, SoundCategory.BLOCKS, 1f, 1f);
            worldIn.notifyBlockUpdate(pos, state, getActualState(state, worldIn, pos), 3);
            switch (canType) {
                case 0:
                    playerIn.addItemStackToInventory(new ItemStack(Holder.green_monster));
                    break;
                case 1:
                    playerIn.addItemStackToInventory(new ItemStack(Holder.yellow_monster));
            }

            tileEntity.checkDespawn();
        }
        else if (heldItem.getItem() == Holder.green_monster && canType == 0 || heldItem.getItem() == Holder.yellow_monster && canType == 1) {
            if (heldItem.hasTagCompound() && heldItem.getTagCompound().getBoolean("friedchickenmod:opened")) return true;
            if (tileEntity.placeCan()) {
                worldIn.playSound(null, pos, Holder.can_take, SoundCategory.BLOCKS, 1f, 1f);
                worldIn.notifyBlockUpdate(pos, state, getActualState(state, worldIn, pos), 3);
                playerIn.getHeldItem(EnumHand.MAIN_HAND).shrink(1);
            }
        }
        return true;
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TilePackOfCans();
    }
}
