package com.complover116.friedchicken;

import com.complover116.friedchicken.blocks.BlockChickenProcessor;
import com.complover116.friedchicken.blocks.BlockPackOfCans;
import com.complover116.friedchicken.items.ItemDrink;
import com.complover116.friedchicken.items.ItemFastFood;
import com.complover116.friedchicken.items.ItemFoodPackaged;
import com.complover116.friedchicken.tileentities.ChickenProcessor;
import com.complover116.friedchicken.tileentities.TilePackOfCans;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import org.apache.logging.log4j.Logger;

@Mod(modid = FriedChickenMod.MODID, name = FriedChickenMod.NAME, version = FriedChickenMod.VERSION)
public class FriedChickenMod
{
    public static final String MODID = "friedchickenmod";
    public static final String NAME = "Fried Chicken";
    public static final String VERSION = "1.0";

    public static Logger logger;

    @SidedProxy(serverSide = "com.complover116.friedchicken.CommonProxy", clientSide = "com.complover116.friedchicken.ClientProxy")
    public static CommonProxy proxy;



    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = event.getModLog();
        MinecraftForge.EVENT_BUS.register(this);

        GameRegistry.registerTileEntity(ChickenProcessor.class, "friedchickenmod:chicken_processor");
        GameRegistry.registerTileEntity(TilePackOfCans.class, "friedchickenmod:pack_of_cans");
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    }

    @SubscribeEvent
    public void registerItems(RegistryEvent.Register<Item> event) {
        Item chickenLeg = new ItemFood(1, 0, false);
        chickenLeg.setUnlocalizedName("friedchickenmod.chicken_leg").setCreativeTab(CreativeTabs.FOOD).setRegistryName("chicken_leg");
        event.getRegistry().register(chickenLeg);

        Item friedChickenLeg = new ItemFastFood(3, 1, 1);
        friedChickenLeg.setUnlocalizedName("friedchickenmod.chicken_leg_fried").setCreativeTab(CreativeTabs.FOOD).setRegistryName("chicken_leg_fried");
        event.getRegistry().register(friedChickenLeg);

        GameRegistry.addSmelting(new ItemStack(chickenLeg), new ItemStack(friedChickenLeg), 1f);

        Item chickenWing = new ItemFood(1, 0, false);
        chickenWing.setUnlocalizedName("friedchickenmod.chicken_wing").setCreativeTab(CreativeTabs.FOOD).setRegistryName("chicken_wing");
        event.getRegistry().register(chickenWing);

        Item friedChickenWing = new ItemFastFood(3, 1, 1);
        friedChickenWing.setUnlocalizedName("friedchickenmod.chicken_wing_fried").setCreativeTab(CreativeTabs.FOOD).setRegistryName("chicken_wing_fried");
        event.getRegistry().register(friedChickenWing);

        Item chickenBreast = new ItemFood(1, 0, false);
        chickenBreast.setUnlocalizedName("friedchickenmod.chicken_breast").setCreativeTab(CreativeTabs.FOOD).setRegistryName("chicken_breast");
        event.getRegistry().register(chickenBreast);

        Item friedChickenBreast = new ItemFastFood(5, 1, 1);
        friedChickenBreast.setUnlocalizedName("friedchickenmod.chicken_breast_fried").setCreativeTab(CreativeTabs.FOOD).setRegistryName("chicken_breast_fried");
        event.getRegistry().register(friedChickenBreast);

        GameRegistry.addSmelting(new ItemStack(chickenLeg), new ItemStack(friedChickenLeg), 1f);
        GameRegistry.addSmelting(new ItemStack(chickenWing), new ItemStack(friedChickenWing), 1f);
        GameRegistry.addSmelting(new ItemStack(chickenBreast), new ItemStack(friedChickenBreast), 1f);

        Item emptyBucket = new Item();
        emptyBucket.setUnlocalizedName("friedchickenmod.bucket_empty").setCreativeTab(CreativeTabs.FOOD).setRegistryName("bucket_empty");
        event.getRegistry().register(emptyBucket);

        Item emptyCan = new Item();
        emptyCan.setUnlocalizedName("friedchickenmod.empty_can").setCreativeTab(CreativeTabs.FOOD).setRegistryName("empty_can");
        event.getRegistry().register(emptyCan);

        Item sugarPack = new Item();
        sugarPack.setUnlocalizedName("friedchickenmod.sugar_pack").setCreativeTab(CreativeTabs.MISC).setRegistryName("sugar_pack");
        event.getRegistry().register(sugarPack);

        ItemFoodPackaged wingBucket = new ItemFoodPackaged(5, 1, emptyBucket, 8);
        wingBucket.setAlwaysEdible();
        wingBucket.setUnlocalizedName("friedchickenmod.bucket_wings").setCreativeTab(CreativeTabs.FOOD).setRegistryName("bucket_wings");
        event.getRegistry().register(wingBucket);

        ItemDrink greenMonster = new ItemDrink(8, emptyCan, 0);
        greenMonster.setUnlocalizedName("friedchickenmod.green_monster").setCreativeTab(CreativeTabs.FOOD).setRegistryName("green_monster");
        event.getRegistry().register(greenMonster);

        ItemDrink yellowMonster = new ItemDrink(8, emptyCan, 1);
        yellowMonster.setUnlocalizedName("friedchickenmod.yellow_monster").setCreativeTab(CreativeTabs.FOOD).setRegistryName("yellow_monster");
        event.getRegistry().register(yellowMonster);

        ItemBlock chickenProcessorBlockItem = new ItemBlock(Holder.chicken_processor);
        chickenProcessorBlockItem.setRegistryName("chicken_processor");
        event.getRegistry().register(chickenProcessorBlockItem);

        ItemBlock packOfCansBlockItem = new ItemBlock(Holder.green_monster_pack);
        packOfCansBlockItem.setRegistryName("green_monster_pack");
        event.getRegistry().register(packOfCansBlockItem);

        ItemBlock packOfCansBlockItem2 = new ItemBlock(Holder.yellow_monster_pack);
        packOfCansBlockItem2.setRegistryName("yellow_monster_pack");
        event.getRegistry().register(packOfCansBlockItem2);
    }

    @SubscribeEvent
    public void registerSounds(RegistryEvent.Register<SoundEvent> event) {
        SoundEvent canOpen = new SoundEvent(new ResourceLocation("friedchickenmod:can_open"));
        canOpen.setRegistryName("can_open");
        event.getRegistry().register(canOpen);

        SoundEvent canDrink = new SoundEvent(new ResourceLocation("friedchickenmod:can_drink"));
        canDrink.setRegistryName("can_drink");
        event.getRegistry().register(canDrink);

        SoundEvent canTake = new SoundEvent(new ResourceLocation("friedchickenmod:can_take"));
        canTake.setRegistryName("can_take");
        event.getRegistry().register(canTake);
    }

    @SubscribeEvent
    public void registerModels(ModelRegistryEvent event) {
        proxy.registerModels();
    }

    @SubscribeEvent
    public void registerBlocks(RegistryEvent.Register<Block> event) {
        Block chickenProcessorBlock = new BlockChickenProcessor();
        event.getRegistry().register(chickenProcessorBlock);

        Block packOfCans = new BlockPackOfCans(0);
        packOfCans.setRegistryName("green_monster_pack");
        packOfCans.setUnlocalizedName("friedchickenmod.green_monster_pack");
        event.getRegistry().register(packOfCans);

        Block packOfCans2 = new BlockPackOfCans(1);
        packOfCans2.setRegistryName("yellow_monster_pack");
        packOfCans2.setUnlocalizedName("friedchickenmod.yellow_monster_pack");
        event.getRegistry().register(packOfCans2);
    }
}
